package me.testslider.app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
* Created by warabei on 20.02.15.
*/
class TopStubAdapter extends RecyclerView.Adapter <TopStubAdapter.TopStubViewHolder>{

	public static final int MIDDLE = Integer.MAX_VALUE / 2;

	private int [] pics = {
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
			R.drawable.back,
			R.drawable.ic_launcher,
	};

	@Override
	public TopStubViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		return new TopStubViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.imageview, viewGroup, false));
	}

	@Override
	public void onBindViewHolder(TopStubViewHolder holder, int position) {
		holder.imageView.setImageResource(getItem(position));
	}

	private int getItem(int position) {
		return pics[position % pics.length];
	}


	@Override
	public int getItemCount() {
		return Integer.MAX_VALUE;
	}

	/**
	* Created by warabei on 20.02.15.
	*/

	static class TopStubViewHolder extends RecyclerView.ViewHolder  {
	
		private final ImageView imageView;
	
		public TopStubViewHolder(View itemView) {
			super(itemView);
			imageView = (ImageView) itemView.findViewById(R.id.imageview);
		}
	}
}
