package me.testslider.app.film;

import me.testslider.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warabei on 20.02.15.
 */
public class FilmItemStub {
	//time in hours
	private int mStartTime;
	private int mEndTime;
	private int mPictureId;

	public FilmItemStub(int mStartTime, int mEndTime, int mPictureId) {
		this.mStartTime = mStartTime;
		this.mEndTime = mEndTime;
		this.mPictureId = mPictureId;
	}

	public int getStartTime() {
		return mStartTime;
	}

	public int getEndTime() {
		return mEndTime;
	}

	public int getPictureId() {
		return mPictureId;
	}


}
