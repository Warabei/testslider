package me.testslider.app.film;

import me.testslider.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warabei on 20.02.15.
 */
public class FilmUtilsStub {
	public static List<FilmItemStub> generateFirstListOfStubs(){
		List<FilmItemStub> filmItemStubs = new ArrayList<FilmItemStub>();
		filmItemStubs.add(new FilmItemStub(0, 1, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(1, 2, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(2, 3, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(3, 4, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(4, 5, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(6, 7, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(7, 8, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(9, 10, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(10, 11, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(11, 12, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(12, 13, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(13, 14, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(14, 15, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(15, 16, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(16, 17, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(17, 18, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(19, 20, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(20, 21, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(21, 22, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(22, 23, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(23, 24, R.drawable.ic_launcher));
		return filmItemStubs;
	}
	public static List<FilmItemStub> generateSecondListOfStubs(){
		List<FilmItemStub> filmItemStubs = new ArrayList<FilmItemStub>();
		filmItemStubs.add(new FilmItemStub(0, 1, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(1, 2, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(2, 3, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(3, 4, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(4, 5, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(6, 7, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(7, 8, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(9, 10, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(10, 11, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(11, 12, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(12, 13, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(13, 14, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(14, 15, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(15, 16, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(16, 17, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(17, 18, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(19, 20, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(20, 21, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(21, 22, R.drawable.back));
		filmItemStubs.add(new FilmItemStub(22, 23, R.drawable.ic_launcher));
		filmItemStubs.add(new FilmItemStub(23, 24, R.drawable.back));
		return filmItemStubs;
	}

	public static int findItemNumberByTime(List<FilmItemStub> filmItemStubs, int hour){
		for (FilmItemStub filmItemStub : filmItemStubs){
			if (filmItemStub.getStartTime() == hour){
				return filmItemStubs.indexOf(filmItemStub);
			}
		}
		return 0;
	}
}
