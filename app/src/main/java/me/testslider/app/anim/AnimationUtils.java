package me.testslider.app.anim;

import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by warabei on 20.02.15.
 */
public class AnimationUtils {

	public static Animation getZoomOutAnimation(){
		Animation zoom = new ScaleAnimation(1f, 1.2f, 1.f, 1.2f, Animation.RELATIVE_TO_SELF,
				0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		zoom.setFillAfter(true);
		zoom.setDuration(100L);
		return zoom;
	}

	public static Animation getZoomInAnimation(){
		Animation zoom = new ScaleAnimation(1.2f, 1f, 1.2f, 1f, Animation.RELATIVE_TO_SELF,
				0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		zoom.setFillAfter(true);
		zoom.setDuration(100L);
		return zoom;
	}

}
