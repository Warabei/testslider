package me.testslider.app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import me.testslider.app.film.FilmItemStub;

import java.util.List;

/**
* Created by warabei on 20.02.15.
*/
class BottomStubAdapter extends RecyclerView.Adapter <BottomStubAdapter.BottomStubViewHolder>{

	private List<FilmItemStub> mItemStubList;

	BottomStubAdapter(List<FilmItemStub> itemStubList) {
		this.mItemStubList = itemStubList;
	}

	@Override
	public BottomStubViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		return new BottomStubViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_w_text, viewGroup, false));
	}

	@Override
	public void onBindViewHolder(BottomStubViewHolder holder, int position) {
		FilmItemStub item = getItem(position);
		holder.imageView.setImageResource(item.getPictureId());
		//stub
		holder.text1.setText(String.format("%d:00-%d:00", item.getStartTime(), item.getEndTime()));
	}

	private FilmItemStub getItem(int position) {
		return mItemStubList.get(position);
	}


	@Override
	public int getItemCount() {
		return mItemStubList.size();
	}

	/**
	* Created by warabei on 20.02.15.
	*/
	static class BottomStubViewHolder extends RecyclerView.ViewHolder  {

		private final ImageView imageView;
		private final TextView text1;
		private final TextView text2;

		public BottomStubViewHolder(View itemView) {
			super(itemView);
			imageView = (ImageView) itemView.findViewById(R.id.image);
			text1 = (TextView) itemView.findViewById(R.id.text1);
			text2 = (TextView) itemView.findViewById(R.id.text2);
		}
	}
}
