package me.testslider.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import me.testslider.app.anim.AnimationUtils;
import me.testslider.app.film.FilmItemStub;
import me.testslider.app.film.FilmUtilsStub;

import java.util.Calendar;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


	private LinearLayoutManager mBottomManager;
	private RecyclerView mBottomRecyclerView;
	private RecyclerView mTopRecyclerView;
	private LinearLayoutManager mTopManager;
	private int mTopChildPosition;

	public MainFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		view.findViewById(R.id.left_button).setOnClickListener(this);
		view.findViewById(R.id.right_button).setOnClickListener(this);

		mBottomRecyclerView = (RecyclerView) view.findViewById(R.id.listview2);
		mBottomManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
		mBottomRecyclerView.setLayoutManager(mBottomManager);

		mTopRecyclerView = (RecyclerView) view.findViewById(R.id.listview);
		mTopManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
		mTopRecyclerView.setLayoutManager(mTopManager);
		mTopRecyclerView.setAdapter(new TopStubAdapter());
		setOnTopScrollListener();
		setStartCenterItem();
	}

	private void startAnimation(View view, Animation animation){
		view.setAnimation(animation);
		animation.startNow();
	}

	private void setStartCenterItem() {
		mTopManager.scrollToPosition(TopStubAdapter.MIDDLE);
		mTopRecyclerView.post(new Runnable() {
			@Override
			public void run() {
				setupTopMiddleItemInCenter();
			}
		});
	}

	private void setOnTopScrollListener() {
		mTopRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
				if (newState == RecyclerView.SCROLL_STATE_IDLE) {
					setupTopMiddleItemInCenter();
				} else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
					zoomInTopCenterView();
				}
			}

		});
	}

	private void zoomInTopCenterView() {
		View centerView = mTopManager.findViewByPosition(mTopChildPosition);

		//по уму нужно умнее трекать, но тестовое
		if (mTopChildPosition == -1 || centerView == null)
			return;

		startAnimation(centerView, AnimationUtils.getZoomInAnimation());
	}

	private void setupTopMiddleItemInCenter() {
		View centerView = mTopRecyclerView.findChildViewUnder(mTopRecyclerView.getWidth() / 2, mTopRecyclerView.getHeight() / 2);
		mTopChildPosition = mTopRecyclerView.getChildPosition(centerView);
		startAnimation(centerView, AnimationUtils.getZoomOutAnimation());

		mTopManager.scrollToPositionWithOffset(mTopChildPosition, mTopRecyclerView.getWidth() / 2 - centerView.getWidth() / 2);
		setBottomAdapterWithCentering(getCurrentTime());
	}

	private void moveTopTo(int shift) {
		View centerView = mTopRecyclerView.findChildViewUnder(mTopRecyclerView.getWidth() / 2, mTopRecyclerView.getHeight() / 2);
		int childPosition = mTopRecyclerView.getChildPosition(centerView);
		View nextCenterView = mTopManager.findViewByPosition(childPosition + shift);
		startAnimation(centerView, AnimationUtils.getZoomInAnimation());

		mTopManager.scrollToPositionWithOffset(childPosition + shift, mTopRecyclerView.getWidth() / 2 - centerView.getWidth() / 2);
		startAnimation(nextCenterView, AnimationUtils.getZoomOutAnimation());
		mTopChildPosition = childPosition + shift;
		setBottomAdapterWithCentering(getCurrentTime());
	}

	private void setBottomAdapterWithCentering(int currentTime) {
		List<FilmItemStub> filmItemStubs = getBottomList();

		mBottomRecyclerView.setAdapter(new BottomStubAdapter(filmItemStubs));

		mBottomManager.scrollToPositionWithOffset(FilmUtilsStub.findItemNumberByTime(filmItemStubs, currentTime), 0);

		mBottomRecyclerView.post(new Runnable() {
			@Override
			public void run() {
				View view = mBottomRecyclerView.findChildViewUnder(0, mBottomRecyclerView.getHeight() / 2);
				int childPosition = mBottomRecyclerView.getChildPosition(view);
				mBottomManager.scrollToPositionWithOffset(childPosition, mBottomRecyclerView.getWidth() / 2 - view.getWidth() / 2);
			}
		});
	}

	//stub для четных и нечетных выводятся картинки по другому, чтобы было видно что меняется
	private List<FilmItemStub> getBottomList() {
		return mTopChildPosition % 2 == 0 ? FilmUtilsStub.generateFirstListOfStubs()
				: FilmUtilsStub.generateSecondListOfStubs();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.left_button:
				moveTopTo(-1);
				break;
			case R.id.right_button:
				moveTopTo(+1);
				break;
		}
	}

	private int getCurrentTime() {
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}


}
